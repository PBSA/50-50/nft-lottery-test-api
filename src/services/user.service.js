const bcrypt = require('bcrypt');

const profileConstants = require('../constants/profile');
const IS_PRODUCTION = process.env.NODE_ENV === 'production';

class UserService {

  /**
     * @param {peerplaysConnection} opts.peerplaysConnection
     */
  constructor(opts) {
    this.peerplaysConnection = opts.peerplaysConnection;
    this.peerplaysRepository = opts.peerplaysRepository;
    this.mailService = opts.mailService;
    this.userRepository = opts.userRepository;

    this.errors = {
      NOT_FOUND: 'NOT_FOUND',
      INVALID_USER_TYPE: 'INVALID_USER_TYPE'
    }
  }

  /**
     * @param {UserModel} User
     * @returns {Promise<UserPublicObject>}
     */
  async getCleanUser(User) {
    return User.getPublic();
  }

  async getUser(id) {
    const User = await this.userRepository.findByPk(id);

    if (!User) {
      throw new Error('User not found');
    }

    return this.getCleanUser(User);
  }

  signUp(req, newUser) {
    newUser.user_type = profileConstants.userType.player;
    return this.createOrUpdateUser(newUser, req.connection.remoteAddress);
  }

  createOrUpdateUser(userData, remoteAddress) {
    if (userData.hasOwnProperty('id')) {
      return this.updateUser(userData, remoteAddress);
    }

    return this.createUser(userData, remoteAddress);
  }

  async createUser(newUser, remoteAddress) {
    const {firstname, lastname, password, email, mobile, organization_id} = newUser;

    let hashedPassword;

    if(password) {
      hashedPassword = await bcrypt.hash(password, 10);
    }

    let peerplaysResult;

    try {
      peerplaysResult = await this.peerplaysRepository.createPeerplaysAccount(email, mobile, password);
    } catch(err) {
      if(err.message.hasOwnProperty('email')) {
        peerplaysResult = await this.peerplaysRepository.signIn(email, mobile, password);
      } else {
        throw err;
      }
    }

    const user = await this.userRepository.model.create({
      email,
      mobile: this.userRepository.normalizePhoneNumber(mobile),
      password: hashedPassword,
      firstname,
      lastname,
      is_email_verified: false,
      is_email_allowed: newUser.is_email_allowed,
      user_type: newUser.user_type,
      status: profileConstants.status.active,
      ip_address: remoteAddress,
      organization_id: newUser.user_type === profileConstants.userType.player ? null : organization_id,
      peerplays_account_name: peerplaysResult.peerplaysAccountName,
      peerplays_account_id: peerplaysResult.peerplaysAccountId
    });

    await this.signInAndJoinApp(user);

    return this.getCleanUser(user);
  }

  async signInAndJoinApp(user) {
    const res = await this.peerplaysRepository.joinApp(user.email, user.mobile, user.password);
    user.access_token = res.token;
    user.refresh_token = res.refresh_token;
    user.token_expires = res.expires;
    user.save();
  }

  async updateUser(updatedUser, remoteAddress) {
    const user = await this.userRepository.findByPk(updatedUser.id);
    if (!user) {
      throw new Error(this.errors.NOT_FOUND);
    }

    if (user.user_type !== updatedUser.user_type) {
      throw new Error(this.errors.INVALID_USER_TYPE);
    }

    user.email = updatedUser.email;
    user.mobile = this.userRepository.normalizePhoneNumber(updatedUser.mobile);

    if (updatedUser.password) {
      user.password = await bcrypt.hash(updatedUser.password, 10);
    }

    user.firstname = updatedUser.firstname;
    user.lastname = updatedUser.lastname;

    if (updatedUser.email !== user.email) {
      user.email = updatedUser.email;
      user.is_email_verified = false;
    }

    user.is_email_allowed = updatedUser.is_email_allowed;

    if(user.user_type !== profileConstants.userType.player) {
      user.organization_id = updatedUser.organization_id;
    }

    user.ip_address = remoteAddress;
    await user.save();

    return this.getCleanUser(user);
  }

  async getSignInUser(email, password) {
    const User = await this.userRepository.findByEmail(email.toLowerCase());

    if (!User) {
      throw new Error('User not found');
    }

    if (!await bcrypt.compare(password, User.password)) {
      throw new Error('Invalid password');
    }

    await this.peerplaysRepository.signIn(email, null, password);

    return this.getCleanUser(User);
  }

  async searchUsers(email, mobile) {
    const User = await this.userRepository.getByEmailOrMobile(email, mobile);

    if (!User) {
      throw new Error('User not found. Please create a new user.');
    }

    await this.peerplaysRepository.signIn(email, mobile);

    return this.getCleanUser(User);
  }
}

module.exports = UserService;

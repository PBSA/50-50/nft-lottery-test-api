const {model} = require('../db/models/beneficiary.model');
const BasePostgresRepository = require('./abstracts/base-postgres.repository');

class BeneficiaryRepository extends BasePostgresRepository {

  constructor() {
    super(model);
  }

}

module.exports = BeneficiaryRepository;
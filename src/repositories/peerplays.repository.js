const BigNumber = require('bignumber.js');
const config = require('config');
BigNumber.config({ROUNDING_MODE: BigNumber.ROUND_FLOOR});

const PeerplaysNameExistsError = require('../errors/peerplays-name-exists.error');

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

class PeerplaysRepository {

  /**
   * @param {PeerplaysConnection} opts.peerplaysConnection
   */
  constructor(opts) {
    this.peerplaysConnection = opts.peerplaysConnection;
  }

  async createPeerplaysAccount(email, mobile, password) {
    try {
      const form = {
        email, mobile
      };

      if(password) {
        form.password = password;
      }

      const {result} = await this.peerplaysConnection.register(form);

      return result;
    } catch (err) {
      if (err.base && err.base[0]) {
        if (err.base[0] === 'Account exists') {
          throw new PeerplaysNameExistsError(`an account with name "${name}" already exists`);
        }
      }

      throw err;
    }
  }

  //seller will sign in with password, while other users will sign in with mobile
  async signIn(email, mobile, password) {
    try {
      const form = {
        login: email
      };

      if(mobile) {
        form.mobile = mobile;
      } else {
        form.password = password;
      }

      const {result} = await this.peerplaysConnection.signIn(form);

      return result;
    } catch (err) {
      throw err;
    }
  }

  randomizeLottoName() {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    for (let i = 0; i < 10; i++) { text += possible.charAt(Math.floor(Math.random() * possible.length)); }

    return text;
  }

  async joinApp(login, mobile, password) {
    try {
      const form = {
        login,
        client_id: config.peerid.client_id,
      };

      if(mobile) {
        form.mobile = mobile;
      } else {
        form.password = password;
      }

      const {result} = await this.peerplaysConnection.joinApp(form);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async refreshAccessToken(refresh_token) {
    try {
      const form = {
        refresh_token,
        client_id: config.peerid.client_id,
        client_secret: config.peerid.client_secret
      };

      const {result} = await this.peerplaysConnection.refreshAccessToken(form);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async createLottery(name, drawDate, owner, is_transferable, is_sellable, delete_tickets_after_draw, progressive_jackpots) {
    var desiredName = name.replace(/[^A-Za-z0-9\s]/gi, '');
    if(desiredName.length > 15) {
      desiredName = desiredName.substr(0,15);
    }
    try {
      const form = {
        operations: [{
          op_name: 'nft_metadata_create',
          fee_asset: config.peerplays.ticketAssetID,
          owner: owner.peerplays_account_id,
          name: desiredName,
          symbol: this.randomizeLottoName(),
          base_uri: ' ',
          is_transferable,
          is_sellable,
          max_supply: Number(config.peerplays.maxTicketSupply),
          lottery_options: {
            benefactors: [{
              id: owner.peerplays_account_id,
              share: new BigNumber(50).shiftedBy(2).toNumber()
            }],
            winning_tickets: [new BigNumber(50).shiftedBy(2).toNumber()],
            ticket_price: {
              amount: new BigNumber(config.peerplays.ticketPrice).toNumber(),
              asset_id: config.peerplays.ticketAssetID
            },
            end_date: Math.floor(new Number(drawDate)/1000), // milliseconds to seconds
            ending_on_soldout: false,
            is_active: true,
            delete_tickets_after_draw,
            progressive_jackpots
          }
        }]
      };

      const {result} = await this.peerplaysConnection.sendOperations(form, owner.access_token);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async updateLotteryImage(owner, peerplaysLotteryId, imageUrl) {
    try {
      const form = {
        operations: [{
          op_name: 'nft_metadata_update',
          fee_asset: config.peerplays.ticketAssetID,
          owner: owner.peerplays_account_id,
          nft_metadata_id: peerplaysLotteryId,
          base_uri: imageUrl
        }]
      };

      const {result} = await this.peerplaysConnection.sendOperations(form, owner.access_token);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async purchaseTicket(draw_id, quantity, player) {
      const operations = [];
      for(let i = 0; i < quantity; i++) {
        operations.push({
          op_name: 'nft_lottery_token_purchase',
          fee_asset: config.peerplays.ticketAssetID,
          lottery_id: draw_id,
          buyer: player.peerplays_account_id,
          tickets_to_buy: 1,
          amount: {
            amount: new BigNumber(config.peerplays.ticketPrice).toNumber(),
            asset_id: config.peerplays.ticketAssetID
          }
        });
      }
      const form = {operations};

      try {
      const {result} = await this.peerplaysConnection.sendOperations(form, player.access_token);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async sendUSD(accountId, amount, sender) {
    let amt = (new BigNumber(amount)).multipliedBy(Math.pow(10,config.peerplays.ticketAssetPrecision));
    amt = Math.round(amt.toNumber());

    const form = {
      operations: [{
        op_name: 'transfer',
        fee_asset: config.peerplays.ticketAssetID,
        from: sender.peerplays_account_id,
        to: accountId,
        amount: {
          amount: amt,
          asset_id: config.peerplays.ticketAssetID
        }
      }]
    };

    try {
      const {result} = await this.peerplaysConnection.sendOperations(form, sender.access_token);

      return result;
    } catch (err) {
      throw err;
    }
  }

  async getWinners(start) {
    return this.peerplaysConnection.getLotteryWinners(start);
  }

  async getUserLotteries(peerplaysAccountId) {
    return this.peerplaysConnection.getUserLotteries(peerplaysAccountId);
  }
}

module.exports = PeerplaysRepository;

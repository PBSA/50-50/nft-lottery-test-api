const { model } = require('../db/models/entry.model');
const BasePostgresRepository = require('./abstracts/base-postgres.repository');

class EntryRepository extends BasePostgresRepository {

  constructor() {
    super(model);
  }

  async findAll(options) {
    return this.model.findAll(options);
  }

}

module.exports = EntryRepository;

const DataTypes = require('sequelize/lib/data-types');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'users',
        'access_token',
        {
          type: DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'users',
        'refresh_token',
        {
          type: DataTypes.STRING,
          allowNull: true
        }
      ),
      queryInterface.addColumn(
        'users',
        'token_expires',
        {
          type: DataTypes.DATE,
          allowNull: true
        }
      ),
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('users','access_token'),
      queryInterface.removeColumn('users','refresh_token'),
      queryInterface.removeColumn('users','token_expires')
    ]);
  }
};
module.exports = {
  transactionType: {
    cashBuy: 'cashBuy',
    stripeBuy: 'stripeBuy',
    ticketPurchase: 'ticketPurchase',
    winnings: 'winnings',
    donations: 'donations'
  }
}
module.exports = {
  organizationType: {
    organization: 'organization',
    beneficiary: 'beneficiary'
  },
  country: {
    us: 'us'
  },
  timeFormat: {
    time12h: '12h',
    time24h: '24h'
  }
}

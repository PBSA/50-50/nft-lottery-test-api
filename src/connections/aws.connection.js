const AWS = require('aws-sdk');
const BaseConnection = require('./abstracts/base.connection');

class AwsConnection extends BaseConnection {

  connect() {
    this.s3 = new AWS.S3();
  }

  disconnect() {}

}

module.exports = AwsConnection;
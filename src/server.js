const {listModules} = require('awilix');
const {getLogger} = require('log4js');

const logger = getLogger();

const {container, initModule} = require('./awilix');

(async () => {
  try {
    const connections = listModules(['src/connections/*.js']);
    await Promise.all(connections.map(async ({name}) => {
      try {
        await container.resolve(name.replace(/\.([a-z])/g, (a) => a[1].toUpperCase())).connect();
      } catch (error) {
        logger.error(`${name} connect error`);
        logger.error(error);
        // process.exit(1);
      }
    }));
    await initModule('api.module');
  } catch (err) {
    logger.error(`Start error: ${err}`);
  } finally {
    logger.info('server has been started');
  }
})();
